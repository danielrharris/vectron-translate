package com.danielrharris.vectrontranslate;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import org.bukkit.entity.Player;
 
public enum MinecraftLocale {
    AUSTRALIAN_ENGLISH("English", "en_AU"),
    ARABIC("Arabic", "ar_SA"),
    BULGARIAN("Bulgarian", "bg_BG"),
    CATALAN("Catalan", "ca_ES"),
    CZECH("Czech", "cs_CZ"),
    DANISH("Danish", "da_DK"),
    GERMAN("German", "de_DE"),
    GREEK("Greek", "el_GR"),
    CANADIAN_ENGLISH("English", "en_CA"),
    ENGLISH("English", "en_US"),
    British_ENGLISH("English", "en_GB"),
    PIRATE_SPEAK("English", "en_PT"),
    ARGENTINEAN_SPANISH("Spanish", "es_AR"),
    SPANISH("Spanish", "es_ES"),
    MEXICO_SPANISH("Spanish", "es_MX"),
    URUGUAY_SPANISH("Spanish", "es_UY"),
    VENEZUELA_SPANISH("Spanish", "es_VE"),
    ESTONIAN("Estonian", "et_EE"),
    FINNISH("Finnish", "fi_FI"),
    FRENCH_CA("French", "fr_CA"),
    FRENCH("French", "fr_FR"),
    HEBREW("Hebrew", "he_IL"),
    HINDI("Hindi", "hi_IN"),
    HUNGARIAN("Hungarian", "hu_HU"),
    INDONESIAN("Indonesian", "id_ID"),
    ITALIAN("Italian", "it_IT"),
    JAPANESE("Japanese", "ja_JP"),
    KOREAN("Korean", "ko_KR"),
    LITHUANIAN("Lithuanian", "lt_LT"),
    LATVIAN("Latvian", "lv_LV"),
    NORWEGIAN("Norwegian", "no_NO"),
    NORWEGIAN1("Norwegian", "nb_NO"),
    DUTCH("Dutch", "nl_NL"),
    PORTUGUESE_BR("Portuguese", "pt_BR"),
    PORTUGUESE_PT("Portuguese", "pt_PT"),
    ROMANIAN("Romanian", "ro_RO"),
    RUSSIAN("Russian", "ru_RU"),
    SLOVAK("Slovak", "sk_SK"), //another weird english, i think it's slovak
    SLOVENIAN("Slovenian", "sl_SI"),
    SWEDISH("Swedish", "sv_SE"),
    THAI("Thai", "th_TH"),
    TURKISH("Turkish", "tr_TR"),
    UKRAINIAN("Ukrainian", "uk_UA"),
    VIETNAMESE("Vietnamese", "vi_VI"),
    SIMPLIFIED_CHINESE("Chinese_simplified", "zh_CN"),
    TRADITIONAL_CHINESE("Chinese_traditional", "zh_TW"),
    POLISH("Polish", "pl_PL");
    
    
    private String name;
    private String code;
    
    MinecraftLocale(String name, String code) {
        this.name = name;
        this.code = code;
    }
    
    public String getName() {
        return name;
    }
    
    public String getCode() {
        return code;
    }
    
    public static String getLanguage(Player p) {
        try {
            Object ep = getMethod("getHandle", p.getClass()).invoke(p, (Object[]) null);
            Field f = ep.getClass().getDeclaredField("locale");
            f.setAccessible(true);
            String languagecode = (String) f.get(ep);
            return getByCode(languagecode);
        }
        catch (Throwable t) {
            t.printStackTrace();
            return null;
        }
    }
    
    private static Method getMethod(String name, Class<?> clazz) {
        for (Method m : clazz.getDeclaredMethods()) {
            if (m.getName().equals(name)) return m;
        }
        return null;
    }
    
    public static String getByCode(String code) {
        for (MinecraftLocale l : values()) {
            if (l.getCode().equalsIgnoreCase(code)) {
                return l.getName();   
            }
            
        }
        return null;
    }
    
}
