# README #

Here ye, here ye!

You have reached the source code for the Vectron Translate bukkit plugin located here http://dev.bukkit.org/bukkit-plugins/vectron-translate/.

Vectron Translate uses the Microsoft bing translator to translate chat between your Minecraft server's players - great for any multilingual server!

Thank you to ZNickQ for the help in initially developing this project, and thank you to bai1 for updating the plugin to 1.7, ironing out some bugs, and adding some great new features!

/DanielRHarris (I can be reached via bukkit private message - I'll try to get back to you as quickly as possible)

Thanks for tuning in!


### Legal stuff: ###
No using my code without my express permission. Blah blah blah. Basically, I would like to keep everything centralized in this one repo, so if you have some bug fix you would like to make, or some great new feature you want to implement, go right ahead! Just submit a pull request when you're done and I'll see about incorporating your change!